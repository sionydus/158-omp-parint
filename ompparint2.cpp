/*
 * =====================================================================================
 *
 *       Filename:  ompparint.cpp
 *
 *    Description:  OpenMP version of intersection function
 *
 *        Version:  1.0
 *        Created:  10/21/2013 07:40:04 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Sunhao Cai (), scai@ucdavis.edu
 *   Organization:  
 *
 * =====================================================================================
 */


#include "ompparint.h"
#include <stdlib.h>
#include <omp.h>
#include <map>
#include <queue>
#include <cstdio>

using namespace std;

typedef map<int, int> CountMap;

struct indexRange {
	int start;
	int end;
};				/* ----------  end of struct indexRange  ---------- */

typedef struct indexRange IndexRange;

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  splitIndices
 *  Description:  
 * =====================================================================================
 */
	IndexRange *
splitIndices ( int n, int numNodes )
{
	IndexRange *indeces = new IndexRange[numNodes];
	
	int chunkSize = n / numNodes;
	if(n % numNodes != 0)
		chunkSize ++;
	// delta is the number of nodes having one less task
	int delta = chunkSize * numNodes - n;

//	printf("delta: %d\n", delta);
//	printf("chunkSize: %d\n", chunkSize);

	// TODO could optimize the arithmetic expression
	for ( int i = 0; i < numNodes ; i ++ ) {
		if(i >= delta){
			indeces[i].start = delta * (chunkSize - 1) + (i - delta) * chunkSize;
			indeces[i].end = delta * (chunkSize - 1) + (i - delta + 1) * chunkSize - 1;
		} else{
			indeces[i].start = i * (chunkSize - 1);
			indeces[i].end = (i + 1) * (chunkSize - 1) - 1;
		}
	}

	return indeces;
}		/* -----  end of function splitIndices  ----- */
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  parint
 *  Description:  
 * =====================================================================================
 */
    void
parint ( int *x, int nx, int *starts, int nstarts, int *rslt, int *nrslt )
{
	int nth;
	IndexRange *ranges;
	
#pragma omp parallel
	{
		int me = omp_get_thread_num();
		nth = omp_get_num_threads();
		
#pragma omp single
		{
			// split up the work
			ranges = splitIndices(nth, nstarts);
		}

		// each node starts working

		// find the smallest set and hash it
		int myFirst = ranges[me].start;
		int myLast = ranges[me].end;
//		int mySize = myLast - myFirst + 1; // inclusive indices

		// if nothing to do
		if(myLast < myFirst)
			return;

		int minFirst = starts[myFirst];
		int minLast ;
		int myMin;

		// if minFirst is the last one, special calculation for length
		if(minFirst == nstarts - 1){
			myMin = nx - minFirst;
			minLast = nx;
		}else{
			myMin = starts[myFirst + 1] - starts[myFirst];
		}

		// if I'm the last node
		if(me == nth - 1){
			myLast --;
		}

		for ( int i = myFirst + 1; i <= myLast; i ++ ) {
			if(starts[i + 1] - starts[i] < myMin){
				myMin = starts[i + 1] - starts[i] ;
			}
		}

		// hash the smallest set
		CountMap cmap;
		for ( int i = minFirst; i < m; {+INCREMENT+} ) {
		}
	}

}		/* -----  end of function parint  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  testSplitIndeces
 *  Description:  
 * =====================================================================================
 */
	void
testSplitIndeces ( int numNodes, int n  )
{
	IndexRange *indeces = splitIndices(n, numNodes);

	for ( int i = 0; i < numNodes; i ++ ) {
		printf("%d, %d\n", indeces[i].start, indeces[i].end);
	}
}		/* -----  end of function testSplitIndeces  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
	testSplitIndeces(10, 3);

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
