all: ompparint
	
ompparint: ompparint.cpp
	g++ -g -fopenmp ompparint.cpp -o ompparint

clean:
	rm -f ompparint
