/*
 * =====================================================================================
 *
 *       Filename:  ompparint.cpp
 *
 *    Description:  OpenMP version of intersection function
 *
 *        Version:  1.0
 *        Created:  10/21/2013 07:40:04 PM
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Sunhao Cai, scai@ucdavis.edu
 *         					Terence Wu,
 *         					Glenn Ryan
 *   Organization:
 *
 * =====================================================================================
 */


#include "ompparint.h"
#include <stdlib.h>
#include <omp.h>
#include <map>
#include <queue>
#include <cstdio>
#include <sstream>
#include <iostream>
#include <queue>
#include <climits>

using namespace std;

typedef map<int, int> CountMap;

struct indexRange {
	int start;
	int end;
};				/* ----------  end of struct indexRange  ---------- */

typedef struct indexRange IndexRange;

typedef queue<int> Pipe;

#define END_MSG INT_MAX

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  splitIndices
 *  Description:	split up the indices among the nodes equally
 * =====================================================================================
 */
	IndexRange *
splitIndices ( int n, int numNodes )
{
	IndexRange *indices = new IndexRange[numNodes];

	int chunkSize = n / numNodes;
	if(n % numNodes != 0)
		chunkSize ++;

	// delta is the number of nodes having one less task
	int delta = chunkSize * numNodes - n;

	// TODO could optimize the arithmetic expression
	for ( int i = 0; i < numNodes ; i ++ ) {
		if(i >= delta){
			indices[i].start = delta * (chunkSize - 1) + (i - delta) * chunkSize;
			indices[i].end = delta * (chunkSize - 1) + (i - delta + 1) * chunkSize - 1;
		} else{
			indices[i].start = i * (chunkSize - 1);
			indices[i].end = (i + 1) * (chunkSize - 1) - 1;
		}
	}

	return indices;
}		/* -----  end of function splitIndices  ----- */

void min_map(map<int,int> &cmap, int& min_first, int &min_last, int nx, int nstarts, int me, int *x, int *starts, IndexRange *ranges) // don't forget to make starts global!
{
	int min_delta;
	int my_first = ranges[me].start;
	int my_last = ranges[me].end;

	//check for case with only one set for the last node
	if( my_first == my_last && my_last == nstarts - 1) {
		min_first = starts[my_first];
		min_last  = nx; //min_last gets last position of smallest set
	}
	else {
		min_first = starts[my_first];
		min_last  = starts[my_first+1];
		min_delta = starts[my_first+1] - starts[my_first];
	}

	// if work size 0, return
	if(min_first > min_last){
		printf("no work\n");
		return;
	}


	//finds start and end index of the smallest set
	for (int i = my_first+1; i <= my_last+1; i++) {
		int first_index;
		int last_index;
		if(i == nstarts) {
			first_index = starts[i-1];
			last_index = nx;
		}
		else {
			first_index = starts[i-1];
			last_index =  starts[i];
		}

		if ((last_index - first_index) < min_delta) {
			min_first = first_index;
			min_last = last_index;
			min_delta = last_index - first_index;
		}
	}

	//stuffs things into the hash table
	for (int i = min_first; i < min_last; cmap[x[i++]] = 1) {
		//		printf("node %d added %d\n", me, x[i]);
	}

}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  parint
 *  Description:	find the intersect of all the given sets in parallel
 * =====================================================================================
 */
	void
parint ( int *x, int nx, int *starts, int nstarts, int **rslt, int *nrslt )
{
	IndexRange *ranges;
	int nth;
	Pipe *pipes;

#pragma omp parallel
	{
		int me = omp_get_thread_num();
		nth = omp_get_num_threads();

	// exit if only 1 thread
	if(nth <= 1){
		cerr << "need at least 2 threads\n";
		exit(0);
	}
#pragma omp single
		{
			/*
			 * split up the work
			 */
			printf("%d threads, %d sets\n.", nth, nstarts);
			ranges = splitIndices(nstarts, nth);
			pipes = new Pipe[nth];
		}


		if(ranges[me].start <= ranges[me].end){
//			printf("node %d: %d to %d\n", me, ranges[me].start, ranges[me].end);

			/*
			 * each node starts working
			 */
			int myFirst = starts[ranges[me].start];

			//last position of the chunk
			int myLast = starts[ranges[me].end];
			if(ranges[me].end == nstarts - 1) {
				myLast = nx; //takes care of last node
			} else { //gets the last position of last set in chunk
				myLast = starts[ranges[me].end + 1];
			}

//			printf("node %d: %d to %d\n", me, myFirst, myLast);

			// hash the smallest set
			CountMap cmap;
			int minFirst, minLast;
			min_map(cmap, minFirst, minLast, nx, nstarts, me, x, starts, ranges);

			// increment counts
			for (int i = myFirst; i < myLast; i++) {
				if (! (minFirst <= i && i < minLast)) {
					if (cmap.count(x[i])){
						cmap[x[i]]++;
					}
				}
			}

			int set_size = minLast - minFirst;
			int num_sets = ranges[me].end - ranges[me].start + 1;

			// the first non-idle node, i.e. the one with work to do
			int first_node = max(0, nth - nstarts);
			if(me == first_node){
//				printf("first_node %d\n", first_node);
				// first node merely sends numbers to the next node
				for ( CountMap::iterator it = cmap.begin(); it != cmap.end(); it++ ) {
					if(it->second == num_sets ){
						pipes[me].push(it->first);
					}
				}

				// sentinel msg
				pipes[me].push(END_MSG);

			}else{
				if(me == nth - 1){
					int num_sets = ranges[me].end - ranges[me].start + 1;
					*rslt = new int[num_sets];
					*nrslt = 0;
				}

				Pipe &prev_pipe = pipes[me - 1];
				while(true){
//					printf("%d, ", me);
					if(prev_pipe.empty())
						continue;

					int p = prev_pipe.front();
					prev_pipe.pop();

					// sentinel
					if(p == END_MSG){
						pipes[me].push(END_MSG);
						break;
					}

					if(cmap.count(p)){
						if(cmap[p] == num_sets){
							if(me == nth - 1){
								// last node receives results from previous node and return result
								(*rslt)[(*nrslt) ++] = p;

							} else{
								// nodes in between receive numbers from previous node and send intersect to the next node
								pipes[me].push(p);
							}
						}
					}
				}
			}
		}else{
			printf("node %d idle\n", me);
		}
	}
}		/* -----  end of function parint  ----- */


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  testSplitindices
 *  Description:
 * =====================================================================================
 */
	void
testSplitindices ( int numNodes, int n  )
{
	IndexRange *indices = splitIndices(n, numNodes);

	for ( int i = 0; i < numNodes; i ++ ) {
		printf("%d, %d\n", indices[i].start, indices[i].end);
	}
}		/* -----  end of function testSplitindices  ----- */

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:
 * =====================================================================================
 */
// 	int
// main ( int argc, char *argv[] )
// {
// 	//	testSplitindices(4, 10);

// 	//                  0               7             13
// 	       int x[74] = {1,2,3,4,5,6,7,  1,2,3,4,5,6,  1,2,3,4,5,
// 		//18            25            31
// 		1,8,3,0,5,4,2,  1,2,3,4,5,6,  1,2,3,4,5,
// 		//36            43            49
// 		1,2,3,4,5,6,7,  1,2,3,4,5,6,  1,2,3,4,5,
// 		//54      58        62      65          70
// 		1,2,3,4,  1,2,3,4,  1,2,3,  9,1,2,3,4,  1,2,3,4};

// 	       int starts[14] = {0,7,13,18,25,31,36,43,49,54,58,62,65,70};
// 	int nx = 74;
// 	int nstarts = 14;
// 	int *rslt, nrslt;

// 	parint(x, nx, starts, nstarts, &rslt, &nrslt);

// 	printf("\nresults\n");
// 	for ( int i = 0; i < nrslt; i ++ ) {
// 		printf("%d ", rslt[i]);
// 	}
// 	cout << endl;

// 	return EXIT_SUCCESS;
// }				/* ----------  end of function main  ---------- */
